ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .gitignore
* .idea/
* .idea/compiler.xml
* .idea/copyright/
* .idea/copyright/profiles_settings.xml
* .idea/misc.xml
* .idea/modules.xml
* .idea/vcs.xml
* .idea/workspace.xml
* README.md
* build/
* build/intermediates/
* build/intermediates/classes/
* build/intermediates/classes/debug/
* build/intermediates/classes/debug/android/
* build/intermediates/classes/debug/android/content/
* build/intermediates/classes/debug/android/content/pm/
* build/intermediates/classes/debug/android/content/pm/IPackageDeleteObserver$Stub$Proxy.class
* build/intermediates/classes/debug/android/content/pm/IPackageDeleteObserver$Stub.class
* build/intermediates/classes/debug/android/content/pm/IPackageDeleteObserver.class
* build/intermediates/classes/debug/android/content/pm/IPackageInstallObserver$Stub$Proxy.class
* build/intermediates/classes/debug/android/content/pm/IPackageInstallObserver$Stub.class
* build/intermediates/classes/debug/android/content/pm/IPackageInstallObserver.class
* build/intermediates/classes/debug/android/content/pm/IPackageStatsObserver$Stub$Proxy.class
* build/intermediates/classes/debug/android/content/pm/IPackageStatsObserver$Stub.class
* build/intermediates/classes/debug/android/content/pm/IPackageStatsObserver.class
* build/intermediates/classes/debug/android/content/pm/PackageManager$NameNotFoundException.class
* build/intermediates/classes/debug/android/content/pm/PackageManager.class
* build/intermediates/classes/debug/android/os/
* build/intermediates/classes/debug/android/os/SystemProperties.class
* build/intermediates/classes/debug/com/
* build/intermediates/classes/debug/com/android/
* build/intermediates/classes/debug/com/android/pm/
* build/intermediates/classes/debug/com/android/pm/BuildConfig.class
* build/intermediates/classes/debug/com/android/pm/R$attr.class
* build/intermediates/classes/debug/com/android/pm/R$drawable.class
* build/intermediates/classes/debug/com/android/pm/R$string.class
* build/intermediates/classes/debug/com/android/pm/R$style.class
* build/intermediates/classes/debug/com/android/pm/R.class
* build/intermediates/classes/release/
* build/intermediates/classes/release/android/
* build/intermediates/classes/release/android/content/
* build/intermediates/classes/release/android/content/pm/
* build/intermediates/classes/release/android/content/pm/IPackageDeleteObserver$Stub$Proxy.class
* build/intermediates/classes/release/android/content/pm/IPackageDeleteObserver$Stub.class
* build/intermediates/classes/release/android/content/pm/IPackageDeleteObserver.class
* build/intermediates/classes/release/android/content/pm/IPackageInstallObserver$Stub$Proxy.class
* build/intermediates/classes/release/android/content/pm/IPackageInstallObserver$Stub.class
* build/intermediates/classes/release/android/content/pm/IPackageInstallObserver.class
* build/intermediates/classes/release/android/content/pm/IPackageStatsObserver$Stub$Proxy.class
* build/intermediates/classes/release/android/content/pm/IPackageStatsObserver$Stub.class
* build/intermediates/classes/release/android/content/pm/IPackageStatsObserver.class
* build/intermediates/classes/release/android/content/pm/PackageManager$NameNotFoundException.class
* build/intermediates/classes/release/android/content/pm/PackageManager.class
* build/intermediates/classes/release/android/os/
* build/intermediates/classes/release/android/os/SystemProperties.class
* build/intermediates/classes/release/com/
* build/intermediates/classes/release/com/android/
* build/intermediates/classes/release/com/android/pm/
* build/intermediates/classes/release/com/android/pm/BuildConfig.class
* build/intermediates/classes/release/com/android/pm/R$attr.class
* build/intermediates/classes/release/com/android/pm/R$drawable.class
* build/intermediates/classes/release/com/android/pm/R$string.class
* build/intermediates/classes/release/com/android/pm/R$style.class
* build/intermediates/classes/release/com/android/pm/R.class
* build/intermediates/dex/
* build/intermediates/dex/release/
* build/intermediates/dex/release/classes.dex
* build/intermediates/libs/
* build/intermediates/libs/PersonalDemoStub-debug.ap_
* build/intermediates/libs/PersonalDemoStub-release.ap_
* build/outputs/
* build/outputs/apk/
* build/outputs/apk/PersonalDemoStub-release-unsigned.apk
* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* assets/ => app/src/main/assets
* res/ => app/src/main/res/
* src/ => app/src/main/java/
* src/android/app/WallpaperInfo.aidl => app/src/main/aidl/android/app/WallpaperInfo.aidl

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
